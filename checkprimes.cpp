#include <iostream>
#include <cmath>
using namespace std;

bool isprime(long long int p){
	bool prime = true;
	long long int mff,divisor; // mff(middle first factor)
	mff = (long long int)sqrt(p)+1;
	divisor = 2; // first factor is 2
	while (divisor < mff && prime == true){
        cout << p << " " << divisor << " " << mff << endl;
		if(p % divisor==0){
			prime = false;
		}
		divisor++;
	}
	return prime;
}


int main(){
long long int n;
int count = 1;
n = 3; //n is the number we check to see if it is prime
bool prime;
cout<<"NUMBER | PRIME COUNT | prime (0=false 1=true)\n";
cout<<"2 \t\t 1"<<endl;
	while (n < 101){
		prime = isprime(n);
		if (prime) {
			count++;
			cout<<"NUMBER | PRIME COUNT | prime (0=false 1=true)\n";
			cout<<n<<"\t"<<count<<"\t"<<prime<<endl;
		}
		n = n + 1;
		
	}
}